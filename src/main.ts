const prettierOptions = {
  bracketSpacing: true,
  overrides: [
      {
          files: ['*.yml'],
          options: {
              printWidth: 160
          }
      },
      {
          files: ['*.astro'],
          options: {
              parser: 'astro'
          },
          plugins: [
              'prettier-plugin-astro'
          ]
      },
      {
          files: ['*.coffee'],
          plugins: [
              'prettier-plugin-coffeescript'
          ]
      },
      {
          files: ['*.css', '*.scss'],
          plugins: [
              'prettier-plugin-rational-order'
          ]
      },
      {
          goTemplateBracketSpacing: true,
          files: ['*.gohtml', '*.gotmpl', '*.tmpl', '*.tpl'],
          plugins: [
              'prettier-plugin-go-template'
          ]
      },
      {
          files: ['*.html', '*.vue'],
          plugins: [
              'prettier-plugin-organize-attributes',
              'prettier-plugin-tailwindcss'
          ]
      },
      {
          files: ['*.java'],
          plugins: [
              'prettier-plugin-java'
          ]
      },
      {
          files: ['*.js', '*.jsx', '*.ts', '*.tsx', '*.mjs'],
          plugins: [
              'prettier-plugin-jsdoc',
              'prettier-plugin-organize-imports'
          ]
      },
      {
          files: ['*.json'],
          plugins: [
              'prettier-plugin-sort-json'
          ]
      },
      {
          files: ['*.liquid'],
          plugins: [
              '@shopify/prettier-plugin-liquid'
          ]
      },
      {
          files: ['*.lua'],
          plugins: [
              '@prettier/plugin-lua'
          ]
      },
      {
          files: ['*.php'],
          plugins: [
              '@prettier/plugin-php'
          ]
      },
      {
          files: ['*.prisma'],
          plugins: [
              'prettier-plugin-prisma'
          ]
      },
      {
          files: ['*.properties'],
          plugins: [
              'prettier-plugin-properties'
          ]
      },
      {
          files: ['*.py'],
          plugins: [
              '@prettier/plugin-python'
          ]
      },
      {
          files: ['*.razor'],
          plugins: [
              'prettier-plugin-razor'
          ]
      },
      {
          files: ['*.rs'],
          plugins: [
              'prettier-plugin-rust'
          ]
      },
      {
          files: ['*.sol'],
          plugins: [
              'prettier-plugin-solidity',
          ]
      },
      {
          files: ['*.tex'],
          plugins: [
              'prettier-plugin-latex'
          ]
      },
      {
          files: ['*.xml'],
          plugins: [
              '@prettier/plugin-xml'
          ]
      },
      {
          files: ['package.json'],
          plugins: [
              'prettier-plugin-package-perfection'
          ]
      },
      {
        files: [
          '.coveragerc',
          '.editorconfig',
          '.flake8',
          '.gitconfig',
          '.gitmodules',
          '.pylintrc',
          '.shellcheckrc',
          '*.cfg',
          '*.cnf',
          '*.dof',
          '*.gitconfig',
          '*.ini',
          '*.lektorproject',
          '*.mc',
          '*.OutJob',
          '*.PcbDoc',
          '*.prefs',
          '*.PrjPCB',
          '*.pro',
          '*.properties',
          '*.reg',
          '*.SchDoc',
          '*.sfv',
          '*.url',
          'buildozer.spec',
          'pylintrc'
        ],
        plugins: [
          'prettier-plugin-ini'
        ]
      },
      {
        files: [
          ".irbrc",
          ".pryrc",
          ".simplecov",
          "*.arb",
          "*.axlsx",
          "*.builder",
          "*.eye",
          "*.fcgi",
          "*.gemfile",
          "*.gemspec",
          "*.god",
          "*.haml",
          "*.jb",
          "*.jbuilder",
          "*.mspec",
          "*.opal",
          "*.pluginspec",
          "*.podspec",
          "*.rabl",
          "*.rake",
          "*.rb",
          "*.rbi",
          "*.rbs",
          "*.rbuild",
          "*.rbw",
          "*.rbx",
          "*.ru",
          "*.ruby",
          "*.thor",
          "*.watchr",
          "Appraisals",
          "Berksfile",
          "Brewfile",
          "buildfile",
          "Buildfile",
          "Capfile",
          "Cheffile",
          "Dangerfile",
          "Deliverfile",
          "Fastfile",
          "Gemfile",
          "Guardfile",
          "Jarfile",
          "Mavenfile",
          "Podfile",
          "Puppetfile",
          "Rakefile",
          "Snapfile",
          "Thorfile",
          "Vagabondfile",
          "Vagrantfile"
        ],
        plugins: [
          '@prettier/plugin-ruby'
        ]
      },
      {
        files: ['*.bash', '*.sh'],
        plugins: [
          'prettier-plugin-sh'
        ]
      }
  ],
  plugins: [
      'prettier-plugin-sql'
  ],
  pluginSearchDirs: false,
  printWidth: 120,
  quoteProps: 'as-needed',
  requirePragma: false,
  semi: false,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'none',
  useTabs: false
}

export default prettierOptions
